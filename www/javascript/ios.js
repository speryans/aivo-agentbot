define(['jquery', 'jquery_depend'], function($) {
	return $.platform.is('iphone') || $.platform.is('ipad') || $.platform.is('ipod');
});