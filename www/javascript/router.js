define([
  'jquery',
  'underscore',
  'backbone',
  'helper/dialogs',
  'views/home/View',
  'jquery_depend'], function($, _, Backbone, Dialogs, HomeView) {

  var AppRouter = Backbone.Router.extend({
    routes: {
      // Custom routes ----------
      'login': 'loginView',

      // End of Custom routes ---

      // Default: all the others
      '*actions': 'defaultAction'
    }
  });

  var initialize = function() {

    Dialogs.init();

    var app_router = new AppRouter;

    // Global var for router
    window.router = app_router;

    app_router.on('route:defaultAction', function(actions) {
      (new HomeView()).render();
    });

    /*app_router.on('route:ideasView', function(actions) {
      if (checkPanel()) {
        (new IdeasView()).render();
      }
    });*/

    Backbone.history.start({
      pushState: false
    });
  };

  return {
    initialize: initialize
  };
});