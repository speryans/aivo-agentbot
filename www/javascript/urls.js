define([], function() {
	return {
		users : '/api/v1/Users',
		todos : '/api/v1/Todoes',
		accounts : '/api/v1/Accounts',
		login : '/api/login',
		add_account : '/api/add_account',
	};
});