define([
  'underscore',
  'backbone',
  'ashe',
  'nuance',
  'helper/dialogs',
  'lang',
/* Templates */
'text!./template.html',
  'text!./row.html',
/* --------- */
'jquery',
  'jquery_depend',
  'jquery_mobile',
  'is!mobile?jquery_scroll',
/* Conditional CSS */
'is!android?css!styles/android',
  'is!wp?css!styles/wp',
/* --------------- */
], function(_, Backbone, Ashe, NuanceSpeechKitPlugin, dialogs, lang, templateData, rowData, $) {

  var View = Backbone.View.extend({
    el: $("#app-page"),

    scrollContent: '.iscroll-content',

    data: [],
    speechKit: new NuanceSpeechKitPlugin(),
    listenning: false,
    playing: false,

    botListUrl: 'https://apibot.agentbot.net/REST/bot/list/?token=23fcD1',

    token: "44c3c08f21876b9967b2d9386a219b1b",

    recognitionType: "dictation",
    language: lang['es'],

    listenIcon: '"',
    listenClass: 'listen',

    talkIcon: '$',
    talkClass: 'success',

    errorIcon: '7',
    errorClass: 'alert',


    // El parámetro answerID debe pasarse en la proxima llamada si y solo si hasPattern > 0, sino siempre tiene por valor 0.
    hasPattern: 0,
    lastAnswerID: 0,
    lastMemory: "",

    // Guardar el ultimo bot usado y cargar el mismo al inicio.
    // Guardar el array de bots
    // O mas simple si el bot no es el mismo que el ultimo no pedir el historial asi sea a memoria.

    originalOpacity: 1,

    uuid: (($.platform.pc) ? "dsadadsas" : device.uuid),

    botName: 'Telefónica',

    initLanguage: function(self) {
      if ($("#micro-button").is(':visible')) {
        $('.left').text(self.language.text);
      } else {
        $('.left').text(self.language.voice);
      }

      $('.right').text(self.language.select_bot);

      $('#message').attr('placeholder', self.language.write_question);
      $('.sendblock span').text(self.language.send);
    },

    render: function() {
      var self = this;

      this.$el.html(Ashe.parse(templateData, {
        title: self.botName
      }));

      this.$el.trigger('create');
      // Init iScroll due to several several bugs
      //if (!$.platform.pc) {
      $(".messages-wrapper").iscrollview();
      //}

      $('#app-page').on('pagebeforechange', function(e) {
        var isScrollable = !$('body').hasClass('preventNativeScroll');
        if (isScrollable) {
          $('body').removeClass('preventNativeScroll');
        } else {
          $('body').addClass('preventNativeScroll');
        }
      });

      // Fixes for Android
      if ($.platform.android) {
        $("header").css('box-shadow', 0);

        // Suscribe to pause event
        document.addEventListener("pause", function() {
          if (self.playing) {
            self.stopMessage();
          }
        }, false);
      }

      if (!$.platform.pc) {
        var class_name = "Credentials";
        if ($.platform.android) {
          class_name = "com.nuance.speechkit.phonegap.Credentials";
        }
        this.speechKit.initialize(class_name, "sandbox.nmdp.nuancemobility.net", 443, false, function(r) {
          console.info(r);
        }, function(e) {
          console.error(e);
        });
      } else {
        $("#micro-button").hide();
        $(".textgrid").show();

        $('.left').hide();
      }

      this.initLanguage(this);

      var lastBot = localStorage.getItem('bot');
      if( !_.isUndefined( lastBot ) ) {
        if( lastBot == this.botName ) {
          this.loadData();
        }
      }

      this.joinEvents();

      return this;
    },

    loadData: function() {
      var self = this;

      $(self.scrollContent).empty();

      dialogs.showLoading();
      // Load old conversation with localStorage hash device.uuid
      $.get("https://apibot.agentbot.net/REST/messages/?hash=" + self.uuid + "&_=1379945945877", function(data) {
        if (data == null || data.length == 0) {
          self.data.push({
            type: 'from',
            answer: self.language.start_conversation
          });
        } else {
          // Just last 3
          data = data.reverse();
          data = data.splice(0, 2);
          data.reverse();

          for (var i in data) {
            // Add answer
            self.data.push({
              type: 'to',
              answer: data[i].text.stripTags()
            });

            // Add response
            self.data.push({
              type: 'from',
              answer: data[i].response.stripTags()
            });
          }
        }

        _.each(self.data, function(row) {
          self.addMessage(self, row);
        });

        dialogs.hideLoading();
        self.scrollPage(self);
      }, 'json');
    },

    playMessage: function(message) {
      var self = this;

      var ttsCallback = function(result) {
        if (result.event == 'TTSStarted') {
          $(".gicon").removeClass(self.listenClass);
          $(".gicon").addClass(self.talkClass);
          $(".gicon").text(self.talkIcon);

          self.playing = true;
        } else if (result.event == 'TTSComplete') {
          $(".gicon").removeClass(self.talkClass);
          $(".gicon").removeClass(self.listenClass);
          $(".gicon").text(self.listenIcon);

          self.playing = false;
        }
      };
      var failureCallback = function(error) {
        $(".gicon").removeClass(self.talkClass);
        $(".gicon").removeClass(self.listenClass);
        $(".gicon").text(self.listenIcon);

        self.playing = false;
      };

      this.speechKit.playTTS(message, self.language.lang, null, ttsCallback, failureCallback);
    },

    stopMessage: function() {
      var self = this;

      var ttsCallback = function(result) {
        self.playing = false;

        $(".gicon").removeClass(self.talkClass);
        $(".gicon").removeClass(self.listenClass);
        $(".gicon").text(self.listenIcon);
      };
      var failureCallback = function(error) {
        self.playing = false;
      };

      this.speechKit.stopTTS(ttsCallback, failureCallback);
    },

    scrollPage: function(self, el) {
      $(".messages-wrapper").iscrollview("refresh");

      if ($(self.scrollContent).height() > $(".messages-wrapper").height()) {
        $(".messages-wrapper").data("mobileIscrollview").iscroll.scrollTo(0, $(self.scrollContent).height(), 100, true);
      }
    },

    addMessage: function(self, msg) {
      $(self.scrollContent).append(Ashe.parse(rowData, msg));

      var sepelement = 'sep' + (new Date()).getTime();

      $(self.scrollContent).append('<div class="separator" id="' + sepelement + '"></div>');

      self.scrollPage(self, "#" + sepelement);
    },

    sendToServer: function(message) {
      var self = this;

      self.addMessage(self, {
        type: 'to',
        answer: message
      });

      var postData = {
        token: self.token,
        hash: self.uuid,
        sentence: message,
        memory: self.lastMemory
      };

      if( self.hasPattern > 0 || self.hasPattern === true ) {
        postData.answerID = self.lastAnswerID;
      } else {
        postData.answerID = 0;
      }

      dialogs.showLoading();
      //console.info("Post Data ==> " + JSON.stringify(postData));
      $.ajax({
        xhr: function() {
          $(".upload-progress").show('slow');
          $(".upload-progress").css('width', '0%');

          var xhr = new window.XMLHttpRequest();
          //Download progress
          xhr.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = (evt.loaded / evt.total) * 100;
              //Do something with download progress
              //alert(percentComplete);

              if (percentComplete > 0 && percentComplete < 100) {
                $(".upload-progress").css('width', percentComplete + '%');
              } else {
                $(".upload-progress").hide('slow');
                $(".upload-progress").css('width', '0%');
              }
            }
          }, false);
          return xhr;
        },
        type: 'POST',
        url: "https://apibot.agentbot.net/REST/answer/",
        data: postData,
        dataType: 'json',
        success: function(data) {
          dialogs.hideLoading();

          console.info("Received: ");
          console.info(JSON.stringify(data));

          if (data.error != "") {
            $(".gicon").addClass(self.errorClass);
            $(".gicon").text(self.errorIcon);
          } else {
            var rm = data.answer;

            if (data.extra.action != undefined) {
              if (data.extra.action == 'PlayVideo') {
                rm = data.answer_clean;
                rm += '<br/><a href="javascript:;" class="play_video" data-url="' + data.extra.param + '">' + self.language.see + '</a>';
              } else if (data.extra.action == "ShowPic") {
                if (data.extra.param.link != undefined) {
                  rm += '</br><a href="javascript:;" class="open_image" data-url="' + data.extra.param.link + '"><div class="minimg" style="background-image: url(' + data.extra.param.img + ');"></div></a>';
                } else {
                  rm += '<a href="javascript:;" class="change_url" data-url="' + data.extra.param + '">' + self.language.open + '</a>';
                }
              } else if (data.extra.action.toLowerCase() == "showurl") {
                rm += '<a href="javascript:;" class="change_url" data-url="' + data.extra.param + '">' + self.language.open + '</a>';
              } else if (data.extra.action.toLowerCase() == 'changeurl') {
                rm += '<a href="javascript:;" class="change_url" data-url="' + data.extra.param + '">' + self.language.open + '</a>';
              }
            }

            if (data.extra.faqs != undefined) {
              var faqs = data.extra.faqs;
              rm = "<strong>FAQ</strong></br>";
              _.each(faqs, function(faq) {
                rm += '<a href="javascript:sendToServer(\'' + faq + '\')">' + faq + '</a></br>';
              });
            }

            if (data.extra.write) {
              //rm = rm.replaceAll('Write(', 'sendToServer(');
              var lines = data.extra.write;
              rm = data.answer_clean;
              _.each(lines, function(line) {
                rm += '<a href="javascript:sendToServer(\'' + line.value + '\')">' + line.label + '</a></br>';
              });
            }

            console.info("Message to show: " + rm);
            self.addMessage(self, {
              type: 'from',
              answer: rm.replaceAll('\n', '<br/>')
            });

            self.lastAnswerID = data.answerID;
            self.lastMemory = data.memory;
            self.hasPattern = data.hasPattern;

            if (data.answer_clean) {
              self.playMessage(data.answer_clean);
            } else {
              self.playMessage(data.answer);
            }
          }
        }
      });
    },

    recognitionCallback: function(self, r) {
      if (r.event == "RecoStarted") {
        $(".gicon").addClass(self.listenClass);

        self.listenning = true;
      } else if (r.event == "RecoComplete") {
        $(".gicon").css('opacity', self.originalOpacity);

        $(".gicon").removeClass(self.talkClass);
        $(".gicon").removeClass(self.listenClass);

        self.listenning = false;

        if (r.result) {
          self.sendToServer(r.result);
        } else {
          self.addMessage(self, {
            type: 'from',
            answer: self.language.no_understand
          });
        }
      } else if (r.event == 'RecoVolumeUpdate') {
        if (r.volumeLevel >= 40) {
          $(".gicon").css('opacity', (r.volumeLevel / 100));
        } else {
          $(".gicon").css('opacity', 50);
        }
      }

      console.info(JSON.stringify(r));
    },

    failureCallback: function(self, e) {
      $(".gicon").css('opacity', self.originalOpacity);

      $(".gicon").removeClass(self.talkClass);
      $(".gicon").removeClass(self.listenClass);
      $(".gicon").addClass(self.errorClass);
      $(".gicon").text(self.errorIcon);

      console.error(JSON.stringify(e));
    },

    joinEvents: function() {
      var self = this;

      window.sendToServer = function(msg) {
        self.sendToServer(msg);
      };

      window.playVideo = function(param) {
        window.open('http://www.youtube.com/watch?v=' + param, '_system');
      };

      window.openUrl = function(url) {
        window.open(url, '_system');
      };

      window.changeBot = function(name, message, token, lng) {
        $(self.scrollContent).empty();

        $("#appTitle").text(name);
        self.botName = name;

        self.language = lang[lng];
        self.initLanguage(self);

        self.addMessage(self, {
          type: 'from',
          answer: message
        });

        self.token = token;

        localStorage.setItem('bot', name);
      };

      $(document).on('tap', '.play_video', function(e) {
        window.open('http://www.youtube.com/watch?v=' + $(this).attr('data-url'), '_system');
        return false;
      });

      $(document).on('tap', '.change_url', function(e) {
        window.open($(this).attr('data-url'), '_system');
        return false;
      });

      $(document).on('load', 'img', function() {
        self.scrollPage(self);
      });

      $(".right").on('tap', function() {
        $(self.scrollContent).empty();

        self.stopMessage();

        dialogs.showLoading();
        $.get(self.botListUrl, function(data) {
          console.info(JSON.stringify(data));
          var msg = "<strong>" + self.language.select_bot + ":</strong></br>";

          _.each(data, function(bot) {
            msg += '<a href="javascript:changeBot(\'' + bot.name + '\', \'' + bot.message + '\', \'' + bot.token + '\', \'' + bot.lang + '\')">' + bot.name + '</a></br>';
          });

          self.addMessage(self, {
            type: 'from',
            answer: msg
          });

          dialogs.hideLoading();
        }, 'json');

        return false;
      });

      $(self.scrollContent).on('tap', '.open_image', function() {
        window.open($(this).attr('data-url'), '_system');

        return false;
      });

      $(".left").on('tap', function() {
        if ($("#micro-button").is(':visible')) {
          $(this).text(self.language.voice);

          $("#micro-button").hide();
          $(".textgrid").show();
        } else {
          $(this).text(self.language.text);

          $(".textgrid").hide();
          $("#micro-button").show();
        }
        return false;
      });

      $("#message").on('focus', function() {
        setTimeout(function() {
          self.scrollPage();
        }, 1500);

        return false;
      });

      $(".sendblock").on('tap', function() {
        if ($("#message").val().trim() == "") {
          dialogs.toast(self.language.no_empty, self.botName);

          return false;
        }

        self.sendToServer($("#message").val());
        $("#message").val('');
        return false;
      });

      $("#micro-button").on('tap', function() {
        if (self.playing) {
          self.stopMessage();
        }

        if (self.listenning) {
          $(".gicon").removeClass(self.listenClass);

          self.speechKit.stopRecognition(function(r) {
            self.recognitionCallback(self, r);
          }, function(e) {
            self.failureCallback(self, e);
          });

          self.listenning = false;
        } else {
          $(".gicon").removeClass(self.errorClass);
          $(".gicon").removeClass(self.talkClass);
          $(".gicon").text(self.listenIcon);

          self.speechKit.startRecognition(self.recognitionType, self.language.lang, function(r) {
            self.recognitionCallback(self, r);
          }, function(e) {
            self.failureCallback(self, e);
          });
        }

        return false;
      });
    },

  });

  return View;

});