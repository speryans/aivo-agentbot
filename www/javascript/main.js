require.config({
  //urlArgs: "bust=" + (new Date()).getTime(), // Just for Development
  paths: {

    pace: 'libs/pace/pace.min',

    jquery: 'libs/jquery/jquery',
    jquery_depend: 'libs/jquery/jquery.depend',
    jquery_transit: 'libs/jquery/jquery.transit.min',
    jquery_mobile: 'libs/jquery/jquery.mobile.min',
    jquery_scroll: 'libs/jquery/jquery.mobile.iscrollview',

    stringjs: 'libs/stringjs/string.min',

    underscore: 'libs/underscore/underscore',
    backbone: 'libs/backbone/backbone',

    /* Require Plugins */
    text: 'libs/require/text',
    is: 'libs/require/is/is',
    'is-api': 'libs/require/is/is-api',
    'is-builder': 'libs/require/is/is-builder',
    css: 'libs/require/css/css',
    normalize: 'libs/require/css/normalize',

    ashe: 'libs/ashe/ashe',

    moment: 'libs/moment/moment.min.spa',

    nuance: 'libs/nuance/nuancespeechkit',

    collections: 'collections',

    helper: 'helpers',

    urls: 'urls',

    styles: '../styles'
  },
  map: {
    '*': {
      'is': 'is',
      'css': 'css'
    }
  },
  shim: {
    'underscore': {
      exports: '_'
    },
    'jquery': {
      exports: '$'
    },
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'jquery_depend': {
      deps: ['jquery']
    },
    'jquery_mobile': {
      deps: ['jquery']
    },
    'jquery_scroll' : {
      deps: ['jquery']
    },
    'ashe': {
      exports: 'Ashe'
    },
    'moment': {
      exports: 'moment'
    },
    'nuance': {
      exports: 'NuanceSpeechKitPlugin'
    }
  }
});

define(['pace'], function(pace) {
  pace.start({
    document: false
  });

  // Load app
  require(['app', 'underscore', 'stringjs', 'jquery', 'jquery_mobile', 'jquery_depend'], function(App, _, S, $) {

    $.mobile.ajaxEnabled = false;
    $.mobile.linkBindingEnabled = false;
    $.mobile.hashListeningEnabled = false;
    $.mobile.pushStateEnabled = false;

    S.extendPrototype();

    App.initialize();
  });

});