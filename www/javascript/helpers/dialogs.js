define(['jquery', 'jquery_mobile'], function($) {

	/*
	In this app OS dialogs are used.
	*/
	return {
		init: function() {
			// Do nothing in this version
		},

		showLoading: function() {
			$.mobile.loading('show');
		},

		hideLoading: function() {
			$.mobile.loading('hide');
		},

		message: function(title, message) {
			navigator.notification.alert(message,function() {}, title);
		},

		toast: function(title, message) {
			navigator.notification.alert(message,function() {}, title);
		},

		ask: function(title, message, callback) {
			navigator.notification.confirm(
			message, // message
			function(buttonIndex) {
				callback( ( buttonIndex == 0 ) );
			}, // callback to invoke
			title, // title
			['Si', 'No']);
		}
	};

});