define([], function() {
	return {
		'en' : {
			lang: 'en_US',
			voice: 'Voice',
			text: 'Text',
			select_bot: 'Select Bot',
			send: 'Send',
			write_question: 'Your question here...',
			no_empty: 'The question cannot be empty',
			no_understand: 'Please repeat your question',
			open: 'Open',
			see: 'See Video',
			start_conversation : 'Press the button or write in the field to start the conversation.'
		},
		'es' : {
			lang: 'es_MX',
			voice: 'Voz',
			text: 'Texto',
			select_bot: 'Elegir Bot',
			send: 'Enviar',
			write_question: 'Ingrese su consulta...',
			no_empty: 'La consulta no puede quedar vacía',
			no_understand: 'No he entendido lo que dijo. Repita por favor.',
			open: 'Abrir',
			see: 'Ver Video',
			start_conversation : 'Presione el botón o escriba para iniciar la conversación.'
		},
		'pt' : {
			lang: 'pt_BR',
			voice: 'Voice',
			text: 'Texto',
			select_bot: 'Escolha Bot',
			send: 'Enviar',
			write_question: 'Digite sua pergunta...',
			no_empty: 'A consulta não pode ser vazio',
			no_understand: 'Eu não entendi o que ele disse. Por favor, repita.',
			open: 'Abrir',
			see: 'Ver Vídeo',
			start_conversation : 'Pressione o botão ou enter para começar a conversa.'
		}
	};
});